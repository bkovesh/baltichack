import React, { Component } from 'react';
import { BrowserRouter, Route, Link, Switch, withRouter } from 'react-router-dom';
import { Ref, Popup, Input, Rating, Accordion, Rail, Sticky, Button, Container, Dropdown, Form, Grid, Header, Card, Dimmer, Icon, Modal, Checkbox, Image, Item, Label, Menu, List, Divider, Segment, Step, Table } from 'semantic-ui-react'
import { connect } from 'react-redux';
//
import {s,cols} from '../../styles/style'
import * as PagesAndContainers from '../'

//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////

//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////

class MenuMain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeItem: null,
      isMobile: window.innerWidth <= 600,
      windowWidth: window.innerWidth,
      windowHeight: window.innerHeight,
      signInOrUp: false,
    };
    this.htmlForDesktop = this.htmlForDesktop.bind(this)
    this.htmlForMobile = this.htmlForMobile.bind(this)
  }
  //////////////////////////////////////////
  handleItemClick = (e, { name, to }) => {
    // console.log(this.props.history)
    this.props.history.push(to)
    this.setState({ activeItem: name })
  }

  updateWindowWidth() {
    if(window.innerWidth < 600) {
      this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight, isMobile:true });
    } else {
      let update_width  = window.innerWidth-100;
      let update_height = Math.round(update_width/4.4);
      this.setState({ windowWidth: update_width, windowHeight: update_height, isMobile: false });
    }
    // console.log(this.state.isMobile)
  }
  //////////////////////////////////////////
  render () {
    const {
      activeItem,
      signInOrUp,
      isMobile,
    } = this.state


    if (isMobile) {
      return (this.htmlForMobile())
    } else {
      return (this.htmlForDesktop())
    }

  }


  //////////////////////////////////////////




  htmlForDesktop = () => {
    const {
      activeItem,
      signInOrUp,
      isMobile,
    } = this.state

    return (
      <Menu
        fixed={'top'} borderless style={s.mn(s.h(50),s.p(0),s.m(0))}
      >
        <Menu.Item as='a' to="/" name='logo' onClick={this.handleItemClick}>
          <Header as="h2">Цифровая трансформация сети очистных сооружений</Header>
        </Menu.Item>


      </Menu>
    )
  }


  htmlForMobile = () => {
    return (
      <Menu
        fixed={'top'} borderless style={s.mn(s.h(50),s.p(0),s.m(0))}
      >
        <Menu.Item as='a' to="/" name='logo' onClick={this.handleItemClick}>
          <Header as="h1">Цифровая трансформация сети очистных сооружений</Header>
        </Menu.Item>
      </Menu>
    )
  }

  //////////////////////////////////////////

  componentDidMount() {
    this.updateWindowWidth();
    window.addEventListener("resize", this.updateWindowWidth.bind(this));
  }


  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowWidth.bind(this));
  }

}



export default withRouter(connect()(MenuMain))
