import React, { Component, createRef } from 'react';
import 'semantic-ui-css/semantic.min.css'
import { Ref, Popup, Input, Rating, Accordion, Rail, Sticky, Button, Container, Dropdown, Form, Grid, Header, Card, Dimmer, Icon, Modal, Checkbox, Image, Item, Label, Menu, List, Divider, Segment, Step, Table } from 'semantic-ui-react'
//
import {config} from '../../config/config'
import {s,cols} from '../../styles/style'
import * as PagesAndContainers from '../'

//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////

class PageMain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isMobile: window.innerWidth <= 600,
      windowWidth: window.innerWidth,
      windowHeight: window.innerHeight,
      allTickets: [],
      signInOrUp: true,
      newPromiseType: true,
      stations: {},
      stationId: 0
    };
    var handleStationId  = this.handleStationId.bind(this);
    this.inputOpenFileRef = createRef()
  }

  updateWindowWidth() {
    if(window.innerWidth < 600) {
      this.setState({ windowWidth: window.innerWidth, windowHeight: window.innerHeight, isMobile:true });
    } else {
      let update_width  = window.innerWidth-100;
      let update_height = Math.round(update_width/4.4);
      this.setState({ windowWidth: update_width, windowHeight: update_height, isMobile: false });
    }
    // console.log(this.state.isMobile)
  }
  //
  render () {
    const {
      isMobile,
    } = this.state

    return (
      <Container fluid style={{padding:'0px 20px'}}>




        {/*<Segment raised>*/}
          <Grid stackable>


            <Grid.Row>

              {/*Map*/}
              <Grid.Column width={11} style={s.mn(s.h('80vh'))} >
                <PagesAndContainers.MapMain handleStationId={this.handleStationId}/>
              </Grid.Column>

              {/*Data*/}
              <Grid.Column width={5} style={isMobile ? s.mn() : s.mn(s.h('80vh'))}>
                {
                  isMobile ? this.htmlListDesktop() : this.htmlListDesktop()
                }
              </Grid.Column>


            </Grid.Row>

          </Grid>
        {/*</Segment>*/}



      </Container>
    );
  }



  htmlListDesktop = () => {
    const {stations, stationId} = this.state
    if (!stations.features) return 'Загрузка...'
    const station = stations.features[stationId]
    console.log('htmlListDesktop',stations.features[stationId])

    return (
      <Container>
        {/*<Header as='h3'>List</Header>*/}

        {/*<Container>*/}
          {/*<Input placeholder='Поиск...' />*/}
        {/*</Container>*/}

        <Header as="h2">{station.properties.title}</Header>

        <Table basic='very' celled selectable>

          <Table.Body>
            <Table.Row>
              <Table.Cell>Владелец</Table.Cell>
              <Table.Cell>
                <Header as='h4'>
                  <Header.Content>
                    {station.properties.supplier.name}
                  </Header.Content>
                </Header>
              </Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Эл.почта</Table.Cell>
              <Table.Cell>{station.properties.supplier.contact_mail}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Справочная служба</Table.Cell>
              <Table.Cell>{station.properties.supplier.contact_phone}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Тариф</Table.Cell>
              <Table.Cell>{station.properties.supplier.tariff}</Table.Cell>
            </Table.Row>
            <Table.Row>
              <Table.Cell>Текущие показания датчиков</Table.Cell>
              <Table.Cell>
                <Header as="h1">
                  {
                    station.properties.phmeter>9 ?
                    <div style={{color:'red'}}>{station.properties.phmeter}</div>:
                    station.properties.phmeter<6 ?
                    <div style={{color:'red'}}>{station.properties.phmeter}</div>:
                    <div style={{color:'black'}}>{station.properties.phmeter}</div>
                  }
                </Header>
              </Table.Cell>
            </Table.Row>
          </Table.Body>
        </Table>

        <Button color='teal'
          onClick={()=>{
            this.props.history.push(`/stations/${stationId}`)
          }}
        >
          Показания
        </Button>

        <Button color='teal'
          onClick={()=>{
            this.props.history.push(`/anomalies/${stationId}`)
          }}
        >
          Аномалии
        </Button>

      </Container>
    )
  }



  componentDidMount() {
    this.updateWindowWidth();
    window.addEventListener("resize", this.updateWindowWidth.bind(this));

    setInterval(()=>{
      // console.log(config.serverDb)
      fetch(`${config.serverDb}/api/edges`)
      .then((res)=>res.json())
      .then((res)=>{
        console.log(res)
        this.setState({stations: this.dataStationsToMap(res)})
      })
      .catch((err)=>console.error(err))
    },2000)

  }


  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowWidth.bind(this));
  }


  // renderStation(id) {
  //   let station = {}
  //   station = this.state.stations.features
  //   if (!station) return 'Загрузка данных...'
  //
  //   return 's'
  // }


  handleStationId(stationId) {
    console.log('handleStationId',stationId)
    // this.setState({
    //   stationId: stationId
    // })
  }


  dataStationsToMap(json) {
    const res = {
      "type": "FeatureCollection",
      "features": []
    }
    json.map((item,i)=>{
      let station = {
        "type": "Feature",
        "properties": {
          "id": `parking-${i}`,
          "icon": "circle",
          "title": item.name,
          "idDb": item.id,
          "phmeter": item.phmeter,
          "score": item.score,
          "supplier": item.supplier,
        },
        "geometry": {
          "type": "Point",
          "coordinates": [item.coord.long,item.coord.lat]
        }
      }
      res.features.push(station)
    })
    // console.log('dataStationsToMap',res)
    return res
  }


}

export default PageMain;
