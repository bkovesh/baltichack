import React, { Component, createRef } from 'react';
import 'semantic-ui-css/semantic.min.css'
import { Ref, Popup, Input, Rating, Accordion, Rail, Sticky, Button, Container, Dropdown, Form, Grid, Header, Card, Dimmer, Icon, Modal, Checkbox, Image, Item, Label, Menu, List, Divider, Segment, Step, Table } from 'semantic-ui-react'
import { connect } from 'react-redux';
//
import {s,cols} from '../../styles/style'
import * as PagesAndContainers from '../'
import * as Acts from '../../store/actions/actionsMain'
import {config} from '../../config/config'

//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////

class PageStation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sensordata: [],
    };
    this.inputOpenFileRef = createRef()
  }
  //
  render () {
    const {
      sensordata,
    } = this.state

    return (
      <Container>
        <Header as='h2'>История</Header>
        <Header as='h3'>показаний датчиков станции</Header>
        {
          sensordata ? this.htmlTable() : 'Загрузка...'
        }
      </Container>
    );
  }



  htmlTable = () => {
    const {sensordata} = this.state
    return (
      <Segment raised>

        <Table basic='very' celled selectable>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Дата</Table.HeaderCell>
              <Table.HeaderCell>Время</Table.HeaderCell>
              <Table.HeaderCell>Значение кислотности, pH</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>

            {
              sensordata ? sensordata.map((item,i)=>{
                if (i>30) return
                return (
                  <Table.Row>
                    <Table.Cell>
                      {
                        new Date(item.date).toLocaleDateString('ru-RU',{
                          year: 'numeric',
                          month: 'long',
                          day: 'numeric'
                        })
                      }
                    </Table.Cell>
                    <Table.Cell>
                      {
                        new Date(item.date).toLocaleDateString('ru-RU',{
                          hour: 'numeric',
                          minute: 'numeric',
                          second: 'numeric'
                        })
                      }
                    </Table.Cell>
                    <Table.Cell>
                      {
                        item.value>9 ?
                        <div style={{color:'red'}}>{item.value}</div>:
                        item.value<6 ?
                        <div style={{color:'red'}}>{item.value}</div>:
                        <div style={{color:'black'}}>{item.value}</div>
                      }
                    </Table.Cell>
                  </Table.Row>
                )
              }) : (
                '...'
              )
            }
          </Table.Body>
        </Table>
      </Segment>
    )
  }




  componentDidMount() {

    setInterval(()=>{
      // console.log(config.serverDb)
      fetch(`${config.serverDb}/api/sensordata/1`)
      .then((res)=>res.json())
      .then((res)=>{
        console.log(res)
        this.setState({sensordata: res})
      })
      .catch((err)=>console.error(err))
    },2000)

  }



}


export default PageStation;

