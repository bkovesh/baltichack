import React, { Component, createRef } from 'react';
import 'semantic-ui-css/semantic.min.css'
import { Ref, Popup, Input, Rating, Accordion, Rail, Sticky, Button, Container, Dropdown, Form, Grid, Header, Card, Dimmer, Icon, Modal, Checkbox, Image, Item, Label, Menu, List, Divider, Segment, Step, Table } from 'semantic-ui-react'
import { connect } from 'react-redux';
//
import {s,cols} from '../../styles/style'
import * as PagesAndContainers from '../'
import * as Acts from '../../store/actions/actionsMain'
import {config} from '../../config/config'

//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////

class PageAnomalies extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anomalies: [],
    };
    this.inputOpenFileRef = createRef()
  }
  //
  render () {
    const {
      anomalies,
    } = this.state

    return (
      <Container>
        <Header as='h2'>Аномалии</Header>
        <Header as='h3'>в показаниях датчиков станции</Header>
        {
          anomalies ? this.htmlTable() : 'Загрузка...'
        }
      </Container>
    );
  }



  htmlTable = () => {
    const {anomalies} = this.state
    return (
      <Segment raised>

        <Table basic='very' celled selectable>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Время начала</Table.HeaderCell>
              <Table.HeaderCell>Время конца</Table.HeaderCell>
              <Table.HeaderCell>Отклонение</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>

            {
              anomalies ? anomalies.map((item,i)=>{
                if (i>30) return
                return (
                  <Table.Row>
                    <Table.Cell>
                      {
                        item.start_dt ? new Date(item.start_dt).toLocaleDateString('ru-RU',{
                          year: 'numeric',
                          month: 'long',
                          day: 'numeric',
                          hour: 'numeric',
                          minute: 'numeric',
                          second: 'numeric'
                        }) : null
                      }
                    </Table.Cell>
                    <Table.Cell>
                      {
                        item.stop_dt ? new Date(item.stop_dt).toLocaleDateString('ru-RU',{
                          year: 'numeric',
                          month: 'long',
                          day: 'numeric',
                          hour: 'numeric',
                          minute: 'numeric',
                          second: 'numeric'
                        }) : null
                      }
                    </Table.Cell>
                    <Table.Cell>
                      {
                        item.deviation
                      }
                    </Table.Cell>
                  </Table.Row>
                )
              }) : (
                '...'
              )
            }
          </Table.Body>
        </Table>
      </Segment>
    )
  }




  componentDidMount() {

    setInterval(()=>{
      // console.log(config.serverDb)
      fetch(`${config.serverDb}/api/anomalies/1`)
      .then((res)=>res.json())
      .then((res)=>{
        console.log(res)
        this.setState({anomalies: res})
      })
      .catch((err)=>console.error(err))
    },2000)

  }



}


export default PageAnomalies;

